import os
import utils1.logging_api as logger  # requires utils/loggin_api.py
import datetime
import traceback


def my_div(x, y):
    logger.write_lo_log(f'started: my_div ({x},{y})', 'DEBUG')
    try:
        result = x / y
    except Exception as e:
        logger.write_lo_log(f'Error in my_div ({x},{y}) : {e} {traceback.format_exc()}', 'ERROR')
        return None
    logger.write_lo_log(f'result of div {result}', 'INFO')
    logger.write_lo_log(f'finished: my_div ({x},{y}) {result}', 'DEBUG')
    return result


def init_logger():
    with open('D:/temp/logs/user.conf') as conf_file:
        lines = conf_file.readlines()
        # log_file_location=D:\temp\logs\ebay.log'\n'
        # log_level=DEBUG
        log_file_location = lines[0].split('=')[1]
        log_level = lines[1].split('=')[1]
        logger.init(f'{log_file_location[0:len(log_file_location) - 1]}' +
                    f'{datetime.datetime.now().year}_' +
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , log_level)


def main():
    init_logger()
    logger.write_lo_log('**************** System started ...', 'INFO')

    logger.write_lo_log('**************** System shutdown ...', 'INFO')
    pass


main()
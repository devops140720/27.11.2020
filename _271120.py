import os
import utils1.logging_api as logger # requires utils/loggin_api.py
import datetime
import traceback
import pymssql
import json

def my_div(x, y):
    logger.write_lo_log(f'started: my_div ({x},{y})', 'DEBUG')
    try:
        result = x / y
    except Exception as e:
        logger.write_lo_log(f'Error in my_div ({x},{y}) : {e} {traceback.format_exc()}', 'ERROR')
        return None
    logger.write_lo_log(f'result of div {result}','INFO')
    logger.write_lo_log(f'finished: my_div ({x},{y}) {result}', 'DEBUG')
    return result

def init_logger():
    with open('D:/temp/logs/user_conf.json') as json_file:
        conf = json.load(json_file)
        #log_file_location=D:\temp\logs\ebay.log'\n'
        #log_level=DEBUG
        logger.init(f'{conf["log_file_location"]}'+
                    f'{datetime.datetime.now().year}_'+
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log_level"])

def test_db_connection():
    try:
        logger.write_lo_log(f'Testing connection to [{conf["server"]}] [{conf["database"]}]', 'INFO')
        conn = pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database'])
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to connecto to db [{conf["server"]}] [{conf["database"]}]', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {e}', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {tr}', 'ERROR')
        print('Faild to connect to db ... exit')
        exit(-1)

def main():
    init_logger()
    logger.write_lo_log('**************** System started ...', 'INFO')

    test_db_connection()

    with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                database=conf['database']) as conn:
        conn.execute_query('SELECT * FROM CUSTOMERS')
        for row in conn:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
        print('=================== was pymssql._mssql connector')

    with pymssql.connect(server=conf['server'], user='', password='',
                                database=conf['database']) as conn:
        cursor = conn.cursor(as_dict=True)
        cursor.execute('SELECT * FROM CUSTOMERS')
        for row in cursor:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
        print('=================== was cursor SELECT * FROM CUSTOMERS')

        cursor.execute('SELECT * FROM CUSTOMERS WHERE NAME=%s','SHALOM')
        for row in cursor:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
        print('=================== was cursor SELECT * FROM CUSTOMERS WHERE NAME=Shalom')

        cursor.execute('SELECT * FROM CUSTOMERS WHERE ID>=%d',1)
        for row in cursor:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
        print('=================== was cursor SELECT * FROM CUSTOMERS WHERE ID>=1')

        '''
        cursor.execute("""
            CREATE PROCEDURE FindCustomer
                @name VARCHAR(100)
            AS BEGIN
                SELECT * FROM CUSTOMERS WHERE NAME = @name
            END 
        """
        )
        conn.commit()
        '''
        cursor.callproc('FindCustomer',('DAVID',))
        for row in cursor:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
        print('=================== was FindCustomer sp (DAVID)')

    logger.write_lo_log('**************** System shutdown ...', 'INFO')
    pass

with open('D:/temp/logs/user_conf.json') as json_file:
    conf = json.load(json_file)

main()